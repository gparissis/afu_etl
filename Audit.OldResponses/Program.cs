﻿using Audit.Domain.Source.Sales.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.OldResponses
{
    class Program
    {
        static void Main(string[] args)
        {
            int greaterThanId = 0;
            var num = 0;
            var files = DiscrepancyFile.GetGreaterThanId(greaterThanId);
            foreach (var file in files)
            {
                Console.WriteLine($"Checking file: {++num} with id:{file.Id}.");
                switch (file.SentOrReceived.ToLower())
                {
                    case "send":
                        var sentRows = file.GetSentRows();
                        Parallel.ForEach(sentRows, new ParallelOptions() { MaxDegreeOfParallelism = 20 }, sent =>
                        {
                            try
                            {
                                var ext = SaleDetailExtension.GetById(sent.SalesDetailExtensionId);
                                if (ext == null) return;
                                var line = Domain.Target.InnoAudit.Entities.InvoiceLine.InsertIfNotExist(ext);
                                var status = line.InsertIfNotCurrentStatus(Domain.Target.InnoAudit.InvoiceStatusEnum.Pending, file.DateSentOrReceived.Value, Domain.Target.InnoAudit.Entities.CommentSource.Innovatix);
                                if (!string.IsNullOrWhiteSpace(sent.OurComment))
                                    status.InsertComment(sent.OurComment, Domain.Target.InnoAudit.Entities.CommentSource.Innovatix, file.DateSentOrReceived.Value);
                            }
                            catch (Exception) { }
                        });
                        break;
                    case "receive":
                        var receivedRows = file.GetReceivedRows();
                        Parallel.ForEach(receivedRows, new ParallelOptions() { MaxDegreeOfParallelism = 20 }, received =>
                        {
                            try
                            {
                                var ext = SaleDetailExtension.GetById(received.SalesDetailExtensionId);
                                if (ext == null) return;
                                var line = Domain.Target.InnoAudit.Entities.InvoiceLine.InsertIfNotExist(ext);
                                Domain.Target.InnoAudit.Entities.InvoiceLineStatus status;
                                if (received.IsResolved)
                                    status = line.InsertIfNotCurrentStatus(Domain.Target.InnoAudit.InvoiceStatusEnum.Resolved, file.DateSentOrReceived.Value, Domain.Target.InnoAudit.Entities.CommentSource.Innovatix);
                                else
                                    status = line.InsertIfNotCurrentStatus(Domain.Target.InnoAudit.InvoiceStatusEnum.Pending, file.DateSentOrReceived.Value, Domain.Target.InnoAudit.Entities.CommentSource.Wholesaler);

                                if (received.IsCreditOrRebillIssued)
                                {
                                    StringBuilder strBuilder = new StringBuilder(received.TheirComment);
                                    strBuilder.AppendLine($"Resolved at {received.GetCreditOrRebillDate}, reference: {received.CreditReference}.");
                                    status.InsertComment(strBuilder.ToString(), Domain.Target.InnoAudit.Entities.CommentSource.Wholesaler, file.DateSentOrReceived.Value);
                                    line.UpdateReconciliation(Convert.ToDateTime(received.CreditDate), received.CreditReference, received.CreditAmount, Convert.ToInt16(received.CreditQuantity),
                                                                Convert.ToDateTime(received.RebillDate), received.RebillReference, received.CreditAmount, Convert.ToInt16(received.CreditQuantity),
                                                                received.NetCreditRebillAmount, file.DateSentOrReceived.Value);
                                    return;
                                }

                                if (!string.IsNullOrWhiteSpace(received.TheirComment))
                                    status.InsertComment(received.TheirComment, Domain.Target.InnoAudit.Entities.CommentSource.Wholesaler, file.DateSentOrReceived.Value);
                            }
                            catch (Exception) { }
                        });
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
