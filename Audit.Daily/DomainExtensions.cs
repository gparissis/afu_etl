﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sales = Audit.Domain.Source.Sales;

namespace Audit.Daily
{
    static class DomainExtensions
    {
        public static Sales.Entities.SaleDetailExtension RemovePreviousAudit(this Sales.Entities.SaleDetailExtension ext)
        {
            using (var ctx = new Domain.Target.InnoAudit.AuditContext())
            {
                var oldLine = ctx.InvoiceLines.SingleOrDefault(x => x.DetailId == ext.SaleDetailId && x.AuditId != ext.Id);                
                if (oldLine != null)
                {
                    oldLine = ctx.InvoiceLines.Include("Statuses").Include("Reconciliation").SingleOrDefault(x => x.DetailId == ext.SaleDetailId && x.AuditId != ext.Id);
                    if (oldLine.LastStatus == Domain.Target.InnoAudit.InvoiceStatusEnum.Open)
                    {
                        oldLine.Statuses.ToList().ForEach(x => ctx.InvoiceLineStatuses.Remove(x));
                        ctx.Reconciliations.Remove(oldLine.Reconciliation);
                        ctx.InvoiceLines.Remove(oldLine);
                        ctx.SaveChanges();
                        return ext;
                    }
                    else
                        return null;
                }
                return ext;
            }
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesInvoiceRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            var detail = ext.Lazy.SaleDetail;
            if (detail.Quantity > 0
                && detail.ItemPrice > 0
                && ext.Contractible == "Y"
                && ext.ContractId.HasValue
                && (!detail.AuditStatus.HasValue || detail.AuditStatus.Value == 1)) return ext;
            return null;
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesMemberRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            var dwCtx = new Domain.Source.InnovatixDW.InnovatixDWContext();
            string[] memberOf = new string[] { "Innovatix", "Innovatix - Limited" };
            var detail = ext.Lazy.SaleDetail;
            var member = ext.Lazy.Member;
            if (memberOf.Contains(member.MemberOf) && (!member.End.HasValue || member.End.Value >= detail.InvoiceDate)) return ext;
            return null;
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesMemberAtWholesalerRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            var dwCtx = new Domain.Source.InnovatixDW.InnovatixDWContext();
            var detail = ext.Lazy.SaleDetail;
            if (!detail.MemberWholesalerAccountId.HasValue)
                return ext;
            else
            {
                var memberAtWholesaler = dwCtx.MembersAtWholesalers.SingleOrDefault(x => x.MemberWholesalerId == detail.MemberWholesalerAccountId.Value);
                if (memberAtWholesaler != null)
                {
                    if (!string.IsNullOrWhiteSpace(memberAtWholesaler.LoadInnovatixEssensaPharmacy)
                    && memberAtWholesaler.LoadInnovatixEssensaPharmacy == "Yes"
                    && !string.IsNullOrWhiteSpace(memberAtWholesaler.Audit)
                    && memberAtWholesaler.Audit == "Yes") return ext;
                }
                return null;
            }
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesAnalysisCodesRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            int[] restricted_for_wholesalers = new int[] { 50700, 50720, 50800, 50820, 50900, 50920, 54000, 55000, 56000 };
            Guid[] wholesalers = new Guid[] { Guid.Parse("4E4EAC8B-DA84-472F-A95D-B977F1144038"), Guid.Parse("EDD349E3-5363-11D4-96F8-00508B6BCEB3") };

            if (ext.AnalysisCode.HasValue)
            {
                if (restricted_for_wholesalers.Contains(ext.AnalysisCode.Value))
                {
                    var sale = ext.Lazy.Sale;
                    if (wholesalers.Contains(sale.SupplierId))
                        return ext;
                    else
                        return null;
                }
                else
                    return ext;
            }
            return null;
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesContractItemRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            var salesCtx = new Domain.Source.Sales.SalesContext();
            var contractItem = ext.Lazy.ContractItem;
            int[] valid_items = new int[] { 372975, 372972, 23182, 23164, 408518, 52773, 403483, 403482, 210567, 402432, 402431, 402430, 188341, 51283, 413227, 413225, 413221, 413222, 413223, 413224, 217529, 383981 };

            if (ext.AnalysisCode.HasValue && contractItem != null)
            {
                var rows = salesCtx.AuditActions.Where(x => x.AnalysisCode == ext.AnalysisCode.Value && x.Report == "Audit Follow-up").ToList();
                var latestVersion = 0;
                if (rows.Count > 0)
                {
                    latestVersion = rows.Max(x => x.Version);
                    var auditActions = salesCtx.AuditActions.Where(x => x.AnalysisCode == ext.AnalysisCode.Value && x.Report == "Audit Follow-up" && x.Version == latestVersion).ToList();
                    if (auditActions.Count > 0)
                    {
                        auditActions = auditActions.Where(x => x.MajorSection == "External").ToList();
                        if (auditActions.Count > 0)
                        {
                            return ext;
                        }
                        else if (ext.ContractItemId.HasValue)
                        {
                            if (!contractItem.VoidedBatchDetailId.HasValue)
                            {
                                if (valid_items.Contains(contractItem.ItemId))
                                {
                                    var detail = ext.Lazy.SaleDetail;
                                    var sale = salesCtx.Sales.Single(x => x.Id == detail.SaleId);
                                    if (sale.SupplierName == "Curascript Inc." && ext.AnalysisCode == 2050)
                                        return ext;
                                }

                            }
                        }
                    }
                }
            }
            return null;
        }

        public static Sales.Entities.SaleDetailExtension SatisfiesContractMemberRules(this Sales.Entities.SaleDetailExtension ext)
        {
            if (ext == null) return null;
            var contractMember = ext.GetContractMember();
            if (contractMember != null)
            {
                if (string.IsNullOrWhiteSpace(contractMember.IsVoided) || contractMember.IsVoided.Trim() == "No")
                    return ext;
            }
            return null;
        }

    }
}
