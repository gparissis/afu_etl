﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Daily
{
    class Logging
    {
        public static void Message(string message)
        {
            var obj = new object();

            lock (obj)
            {
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    w.WriteLine($"{DateTime.Now}, {message}");
                }
            }            
        }

        public static void Exception(Exception ex)
        {
            var obj = new object();

            lock (obj)
            {
                try
                {
                    using (StreamWriter w = File.AppendText("log.txt"))
                    {
                        StringBuilder strBuilder = new StringBuilder();
                        strBuilder.Append(ex.Message);
                        while (ex.InnerException != null)
                        {
                            ex = ex.InnerException;
                            strBuilder.Append(ex.Message);
                        }
                        strBuilder.AppendLine($"Stack: {ex.StackTrace}");

                        w.WriteLine($"{DateTime.Now}, {strBuilder.ToString()}");
                    }
                }
                catch (Exception) { }
            }
        }
    }
}
