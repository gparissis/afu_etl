﻿using Audit.Domain.Source.Sales.Entities;
using Audit.Domain.Target.InnoAudit.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Audit.Daily
{
    class Program
    {
        static void Main(string[] args)
        {
            int tasksNum = Convert.ToInt32(args[0]);

            var ctx = new Domain.Target.InnoAudit.AuditContext();
            ctx.Database.ExecuteSqlCommand("alter index all on afu.InvoiceLineSummaryRows DISABLE");
            ctx.Database.ExecuteSqlCommand("alter index all on afu.InvoiceLines DISABLE");
            ctx.Database.ExecuteSqlCommand("alter index IX_InvoiceLines_CreatedAt on afu.InvoiceLines REBUILD");
            ctx.Database.ExecuteSqlCommand("alter index PK_InvoiceLines_Id on afu.InvoiceLines REBUILD");
            ctx.Database.ExecuteSqlCommand("alter index IX_InvoiceLines_DetailId on afu.InvoiceLines REBUILD");

            while ((DateTime.Now - Properties.Settings.Default.LastAuditChecked).Days > 1)
            {
                var start = Stopwatch.StartNew();
                var auditDate = Properties.Settings.Default.LastAuditChecked.AddDays(1);
                var lines = SaleDetailExtension.GetExtensionsByAuditDate(auditDate);
                int failed = 0;
                Console.WriteLine(auditDate);

                Parallel.ForEach(lines,
                    new ParallelOptions() { MaxDegreeOfParallelism = tasksNum },
                    line =>
                   {
                       try
                       {
                           var validLine = line.RemovePreviousAudit()
                                                 .SatisfiesInvoiceRules()?
                                                 .SatisfiesMemberRules()?
                                                 .SatisfiesMemberAtWholesalerRules()?
                                                 .SatisfiesAnalysisCodesRules()?
                                                 .SatisfiesContractItemRules()?
                                                 .SatisfiesContractMemberRules();

                           if (validLine == null) return;
                           InvoiceLine.InsertIfNotExist(validLine)
                                        .InsertIfNoneStatus(Audit.Domain.Target.InnoAudit.InvoiceStatusEnum.Open);
                       }
                       catch (Exception ex)
                       {
                           Interlocked.Increment(ref failed);
                           Logging.Exception(ex);
                       }
                   });


                // insert all vaLidLIne data into a staging table
                var busyTime = start.Elapsed.TotalSeconds;
                Logging.Message($"{auditDate}, sec: {busyTime}, total: {lines.Count}, failed: {failed}.");
                Properties.Settings.Default.LastAuditChecked = auditDate;
                Properties.Settings.Default.Save();

                ctx.Database.ExecuteSqlCommand("alter index PK_InvoiceLineSummaryRows on afu.InvoiceLineSummaryRows REBUILD");
                ctx.Database.ExecuteSqlCommand("alter index all on afu.InvoiceLineSummaryRows REBUILD");
                ctx.Database.ExecuteSqlCommand("alter index all on afu.InvoiceLines REBUILD");                
            }
        }
    }
}
