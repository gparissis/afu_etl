﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit
{
    public enum InvoiceStatusEnum : byte
    {
        Open = 0,
        Pending = 1,
        Resolved = 2,
        Closed = 3        
    }
}
