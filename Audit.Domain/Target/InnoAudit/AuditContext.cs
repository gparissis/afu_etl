﻿using Audit.Domain.Target.InnoAudit.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit
{
    public class AuditContext : DbContext
    {
        public DbSet<Item> Items { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Wholesaler> Wholesalers { get; set; }
        public DbSet<InvoiceLine> InvoiceLines { get; set; }
        public DbSet<Entities.InvoiceLineStatus> InvoiceLineStatuses { get; set; }
        public DbSet<InvoiceLineStatusComment> InvoiceLineStatusComments { get; set; }
        public DbSet<Reconciliation> Reconciliations { get; set; }

        public AuditContext() : base("audit_db")
        {
            Database.SetInitializer<AuditContext>(null);
            Database.CommandTimeout = 180;
        }

        public override int SaveChanges()
        {
            var added = ChangeTracker.Entries().Where(x => x.State == EntityState.Added).ToList();
            foreach (var item in added)
            {
                BaseEntity entity = (BaseEntity)item.Entity;
                entity.CreatedAt = entity.CreatedAt==DateTime.MinValue ? DateTime.Now : entity.CreatedAt ;
                entity.UpdatedAt = entity.UpdatedAt == DateTime.MinValue ? DateTime.Now : entity.UpdatedAt;                
                if (entity is BaseCreatedUpdatedByEntity)
                {
                    ((BaseCreatedUpdatedByEntity)entity).CreatedBy = "AutoFeed";
                    ((BaseCreatedUpdatedByEntity)entity).UpdatedBy = "AutoFeed";
                }
            }

            var modified = ChangeTracker.Entries().Where(x => x.State == EntityState.Modified).ToList();
            foreach (var item in modified)
            {
                BaseEntity entity = (BaseEntity)item.Entity;
                entity.UpdatedAt = entity.UpdatedAt == DateTime.MinValue ? DateTime.Now : entity.UpdatedAt;
            }
            return base.SaveChanges();
        }
    }
}
