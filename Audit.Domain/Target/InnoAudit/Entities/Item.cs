﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("Items", Schema = "afu")]
    public class Item: BaseEntity
    {

        #region properties

        public int SourceId { get; set; }

        [Required, MaxLength(100)]
        public string Tradename { get; set; }

        [MaxLength(11)]
        public string NDC { get; set; }

        [MaxLength(60)]
        public string Form { get; set; }

        [MaxLength(60)]
        public string Strength { get; set; }

        [MaxLength(250)]
        public string LabelerAbbreviation { get; set; }

        [MaxLength(50)]
        public string PackageSize { get; set; }

        [MaxLength(12)]
        public string UPC { get; set; }

        [MaxLength(100)]
        public string VendorNumber { get; set; }

        #endregion

        #region static functionality

        public static Item GetBySourceId(int sourceId)
        {
            using (var ctx = new AuditContext())
            {
                return ctx.Items.SingleOrDefault(x => x.SourceId == sourceId);
            }
        }

        public static Item InsertOrUpdate(Domain.Source.InnovatixDW.Entities.Item sourceItem)
        {
            var item = GetBySourceId(sourceItem.Id);            
            using (var ctx = new AuditContext())
            {
                if (item==null)
                {
                    item = new Domain.Target.InnoAudit.Entities.Item();
                    item.Id = Guid.NewGuid();
                    item.SourceId = sourceItem.Id;
                    ctx.Items.Add(item);
                }

                item.Form = sourceItem.Form;
                item.LabelerAbbreviation = sourceItem.Labeler;
                item.NDC = sourceItem.NDC;
                item.PackageSize = sourceItem.PackageSize;
                item.Strength = sourceItem.Strength;
                item.Tradename = sourceItem.Name;
                item.UPC = sourceItem.UPC;
                item.VendorNumber = sourceItem.NumberAtVendor;
                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception) { }
            }
            return item;
        }

        public static Item InsertOrUpdate(Domain.Source.Sales.Entities.SaleDetailExtension ext)
        {
            return InsertOrUpdate(ext.Lazy.Item);
        }
        #endregion
    }
}
