﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("Reconciliations", Schema = "afu")]
    public class Reconciliation : BaseCreatedUpdatedByEntity
    {
        [Required, ForeignKey("Id")]
        public InvoiceLine InvoiceLine { get; set; }
        public bool CreditIssued { get; set; }
        public DateTime? CreditDate { get; set; }
        [MaxLength(50)]
        public string CreditReferenceNumber { get; set; }
        public decimal? CreditAmount { get; set; }
        public Int16? CreditQuantity { get; set; }
        public bool RebillIssued { get; set; }
        public DateTime? RebillDate { get; set; }
        [MaxLength(50)]
        public string RebillReferenceNumber { get; set; }
        public decimal? RebillAmount { get; set; }
        public Int16? RebillQuantity { get; set; }
        public decimal? NetCreditRebillAmount { get; set; }
    }
}
