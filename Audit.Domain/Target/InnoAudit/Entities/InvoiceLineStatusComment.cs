﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("InvoiceLineStatusComments", Schema = "afu")]
    public class InvoiceLineStatusComment : BaseCreatedUpdatedByEntity
    {
        #region properties

        [Required]
        public Guid InvoiceLineStatusId { get; set; }

        [Required]
        public CommentSource Source { get; set; }

        [Required, MaxLength(255)]
        public string Content { get; set; }

        [Required]
        public bool IsVisibleToMember { get; set; }

        [Required]
        public bool IsVisibleToWholesaler { get; set; }

        public InvoiceLineStatus InvoiceStatus { get; set; }

        #endregion
    }

    public enum CommentSource : byte
    {
        Innovatix = 0,
        Wholesaler = 1
    }

}
