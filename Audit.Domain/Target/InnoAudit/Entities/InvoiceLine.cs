﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("InvoiceLines", Schema = "afu")]
    public class InvoiceLine : BaseCreatedUpdatedByEntity
    {
        #region properties

        [Required]
        public Int64 DetailId { get; set; }

        [Required]
        public Int64 AuditId { get; set; }

        [Required]
        public Guid MemberId { get; set; }

        [Required]
        public Guid WholesalerId { get; set; }

        [Required]
        public Guid ItemId { get; set; }

        public string MemberAccountNumberByWholesaler { get; set; }

        public string MemberLevelAtWholesaler { get; set; }

        public string ItemNumberAtVendor { get; set; }

        public string ItemNumberAtWholesaler { get; set; }

        public string InvoiceNumber { get; set; }

        [Required]
        public DateTime InvoiceDate { get; set; }

        public DateTime? PricingDate { get; set; }

        public string PurchaseType { get; set; }

        public int Quantity { get; set; }

        public decimal? WACOnInvoiceDate { get; set; }

        public decimal? ContractPriceByInnovatix { get; set; }

        public decimal? UnitPrice { get; set; }

        public decimal Difference { get; set; }

        public decimal ItemTotalPrice { get; set; }

        public string ContractNumberByVendor { get; set; }

        public string ContractNumberByInnovatix { get; set; }

        [Required]
        public DateTime AuditDate { get; set; }

        public int AnalysisCode { get; set; }

        public string Analysis { get; set; }

        public string OriginalAnalysis { get; set; }

        public bool IsEligibilityVerified { get; set; }

        public bool IsPriceVerified { get; set; }

        public bool IsCreditOrRebillIssued { get; set; }

        public DateTime? CreditOrRebillDate { get; set; }

        public string CreditOrRebillReferenceNumber { get; set; }

        public InvoiceStatusEnum LastStatus { get; set; }

        public DateTime LastActivity { get; set; }

        public byte LastActivitySource { get; set; }

        public Reconciliation Reconciliation { get; set; }

        public List<InvoiceLineStatus> Statuses { get; set; }

        #endregion

        #region static functionality

        public static InvoiceLine GetByAuditId(Int64 auditId)
        {
            using (var ctx = new AuditContext())
            {
                return ctx.InvoiceLines.SingleOrDefault(x => x.AuditId == auditId);
            }
        }

        public static InvoiceLine GetById(Guid id)
        {
            using (var ctx = new AuditContext())
            {
                return ctx.InvoiceLines.SingleOrDefault(x => x.Id == id);
            }
        }

        public static InvoiceLine InsertIfNotExist(Domain.Source.Sales.Entities.SaleDetailExtension ext)
        {
            var line = InvoiceLine.GetByAuditId(ext.Id);
            if (line != null) return line;

            using (var ctx = new AuditContext())
            {
                line = new InvoiceLine();
                line.Id = Guid.NewGuid();
                line.AuditId = ext.Id;
                line.DetailId = ext.Lazy.SaleDetail.Id;
                line.LastStatus = InvoiceStatusEnum.Open;
                line.LastActivity = DateTime.Now;
                line.LastActivitySource = 0;
                line.Analysis = ext.Analysis;
                line.AnalysisCode = ext.AnalysisCode.HasValue ? ext.AnalysisCode.Value : 0;
                line.AuditDate = ext.GetAuditBatch().AuditedAt;
                line.ContractNumberByInnovatix = ext.GetOurContract()?.Number;
                line.ContractNumberByVendor = ext.GetVendorContract()?.VendorContract;
                line.ContractPriceByInnovatix = ext.Lazy.ContractItem?.Price;
                line.Difference = (ext.Lazy.SaleDetail.ItemPrice - ext.Lazy.ContractItem?.Price) * ext.Lazy.SaleDetail.Quantity ?? 0;
                line.InvoiceNumber = ext.Lazy.SaleDetail.InvoiceNumber;
                line.ItemId = Item.InsertOrUpdate(ext.Lazy.Item).Id;
                line.InvoiceDate = ext.Lazy.SaleDetail.InvoiceDate;
                line.ItemNumberAtVendor = ext.Lazy.Item.NumberAtVendor;
                line.ItemNumberAtWholesaler = ext.Lazy.SaleDetail.ItemNumberAtWholesaler;
                line.ItemTotalPrice = ext.Lazy.SaleDetail.ItemPrice * ext.Lazy.SaleDetail.Quantity;
                line.Quantity = ext.Lazy.SaleDetail.Quantity;
                line.MemberAccountNumberByWholesaler = ext.GetMemberAtWholesaler()?.AccountNumber;
                line.MemberLevelAtWholesaler = ext.GetMemberAtWholesaler()?.MemberLevel;
                line.OriginalAnalysis = "";
                line.PricingDate = ext.Lazy.SaleDetail.PricingDate;
                line.PurchaseType = ext.GetPurchaseType().Name;
                line.UnitPrice = ext.Lazy.SaleDetail.ItemPrice;
                line.WACOnInvoiceDate = ext.Lazy.Item.GetWACPriceAtInvoiceDate(ext.Lazy.SaleDetail.InvoiceDate)?.Price;
                line.WholesalerId = Wholesaler.InsertOrUpdate(ext).Id;
                line.MemberId = Member.InsertOrUpdate(ext).Id;

                var reconciliation = new Reconciliation() { Id = line.Id };
                line.Reconciliation = reconciliation;
                ctx.InvoiceLines.Add(line);
                ctx.SaveChanges();
            }
            return line;
        }
        #endregion

        #region functionality

        public InvoiceLineStatus GetCurrentStatus()
        {
            using (var ctx = new AuditContext())
            {
                return ctx.InvoiceLineStatuses.Where(x => x.InvoiceLineId == Id).OrderByDescending(x => x.CreatedAt).FirstOrDefault();
            }
        }

        public InvoiceLineStatus InsertStatus(InvoiceStatusEnum status)
        {
            using (var ctx = new AuditContext())
            {
                var lastStatus = new InvoiceLineStatus();
                lastStatus.Id = Guid.NewGuid();
                lastStatus.InvoiceLineId = Id;
                lastStatus.Status = status;
                ctx.InvoiceLineStatuses.Add(lastStatus);
                ctx.InvoiceLines.Attach(this);
                if (this.LastStatus != status)
                {
                    ctx.Entry(this).State = System.Data.Entity.EntityState.Modified;
                    this.LastActivity = DateTime.Now;
                    this.LastStatus = status;
                    this.LastActivitySource = 0;
                }
                ctx.SaveChanges();
                return lastStatus;
            }
        }

        public InvoiceLineStatus InsertStatus(InvoiceStatusEnum status, DateTime createdAt, CommentSource source)
        {
            using (var ctx = new AuditContext())
            {
                var lastStatus = new InvoiceLineStatus();
                lastStatus.Id = Guid.NewGuid();
                lastStatus.InvoiceLineId = Id;
                lastStatus.Status = status;
                lastStatus.CreatedAt = createdAt;
                lastStatus.UpdatedAt = createdAt;
                ctx.InvoiceLineStatuses.Add(lastStatus);
                if (this.LastStatus != status)
                {
                    ctx.InvoiceLines.Attach(this);
                    ctx.Entry(this).State = System.Data.Entity.EntityState.Modified;
                    this.LastActivity = createdAt;
                    this.LastStatus = status;
                    this.LastActivitySource = (byte)source;
                }
                ctx.SaveChanges();
                return lastStatus;
            }
        }

        public InvoiceLineStatus InsertIfNotCurrentStatus(InvoiceStatusEnum status)
        {
            var lastStatus = GetCurrentStatus();
            if (lastStatus == null || lastStatus.Status != status)
            {
                return InsertStatus(status);
            }
            return lastStatus;
        }

        public InvoiceLineStatus InsertIfNotCurrentStatus(InvoiceStatusEnum status, DateTime createdAt, CommentSource source)
        {
            var lastStatus = GetCurrentStatus();
            if (lastStatus == null || lastStatus.Status != status)
            {
                return InsertStatus(status, createdAt, source);
            }
            return lastStatus;
        }

        public InvoiceLineStatus InsertIfNoneStatus(InvoiceStatusEnum status)
        {
            var lastStatus = GetCurrentStatus();
            if (lastStatus == null)
            {
                return InsertStatus(status);
            }
            return lastStatus;
        }

        public Reconciliation UpdateReconciliation(DateTime? creditDate, string creditReference, decimal? creditAmount, Int16? creditQuantity,
                                                    DateTime? rebillDate, string rebillReference, decimal? rebillAmount, Int16? rebillQuantity,
                                                    decimal? netCreditRebillAmount, DateTime createdAt)
        {
            using (var ctx = new AuditContext())
            {
                Reconciliation reconciliation = ctx.Reconciliations.SingleOrDefault(x => x.Id == this.Id);
                if (reconciliation == null)
                {
                    reconciliation = new Reconciliation();
                    reconciliation.Id = this.Id;
                    reconciliation.CreatedAt = createdAt;
                    ctx.Reconciliations.Add(reconciliation);
                }
                reconciliation.Id = this.Id;
                reconciliation.CreditIssued = reconciliation.CreditDate.HasValue ? true : false;
                reconciliation.CreditDate = creditDate;
                reconciliation.CreditReferenceNumber = creditReference;
                reconciliation.CreditAmount = creditAmount.HasValue ? Math.Abs(creditAmount.Value) : 0;
                reconciliation.CreditQuantity = creditQuantity;
                reconciliation.RebillIssued = reconciliation.RebillDate.HasValue ? true : false;
                reconciliation.RebillDate = rebillDate;
                reconciliation.RebillReferenceNumber = rebillReference;
                reconciliation.RebillAmount = rebillAmount.HasValue ? Math.Abs(rebillAmount.Value) : 0;
                reconciliation.RebillQuantity = rebillQuantity;
                reconciliation.NetCreditRebillAmount = netCreditRebillAmount;
                reconciliation.CreatedAt = createdAt;
                ctx.Reconciliations.Add(reconciliation);
                ctx.SaveChanges();
                return reconciliation;
            }
        }

        public Reconciliation InsertCreditIfNotExist(DateTime? creditDate, string creditReference, decimal? creditAmount, Int16? creditQuantity, DateTime createdAt)
        {
            using (var ctx = new AuditContext())
            {
                Reconciliation reconciliation = ctx.Reconciliations.SingleOrDefault(x => x.Id == this.Id);
                if (reconciliation == null)
                {
                    reconciliation = new Reconciliation();
                    reconciliation.Id = this.Id;
                    reconciliation.CreatedAt = createdAt;
                    ctx.Reconciliations.Add(reconciliation);
                }
                reconciliation.CreditIssued = true;
                reconciliation.CreditDate = creditDate;
                reconciliation.CreditReferenceNumber = creditReference;
                reconciliation.CreditAmount = creditAmount;
                reconciliation.CreditQuantity = creditQuantity;
                reconciliation.NetCreditRebillAmount = (reconciliation.CreditAmount ?? 0) - (reconciliation.RebillAmount ?? 0);
                ctx.SaveChanges();
                return reconciliation;
            }
        }

        public Reconciliation InsertRebillIfNotExist(DateTime? rebillDate, string rebillReference, decimal? rebillAmount, Int16? rebillQuantity, DateTime createdAt)
        {
            using (var ctx = new AuditContext())
            {
                Reconciliation reconciliation = ctx.Reconciliations.SingleOrDefault(x => x.Id == this.Id);
                if (reconciliation == null)
                {
                    reconciliation = new Reconciliation();
                    reconciliation.Id = this.Id;
                    reconciliation.CreatedAt = createdAt;
                    ctx.Reconciliations.Add(reconciliation);
                }
                reconciliation.RebillIssued = true;
                reconciliation.RebillDate = rebillDate;
                reconciliation.RebillReferenceNumber = rebillReference;
                reconciliation.RebillAmount = rebillAmount;
                reconciliation.RebillQuantity = rebillQuantity;
                reconciliation.NetCreditRebillAmount = (reconciliation.CreditAmount ?? 0) - (reconciliation.RebillAmount ?? 0);
                ctx.SaveChanges();
                return reconciliation;
            }
        }
        #endregion
    }

}
