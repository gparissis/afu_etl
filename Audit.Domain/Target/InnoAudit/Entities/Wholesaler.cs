﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("Wholesalers", Schema = "afu")]
    public class Wholesaler : BaseEntity
    {
        #region properties

        [Required, MaxLength(100)]
        public string Name { get; set; }

        #endregion

        #region static functionality

        public static Wholesaler GetById(Guid id)
        {
            using (var ctx=new AuditContext())
            {
                return ctx.Wholesalers.SingleOrDefault(x => x.Id == id);
            }
        }

        public static Wholesaler InsertOrUpdate(Domain.Source.Sales.Entities.Sale sale)
        {
            var wholesaler = Wholesaler.GetById(sale.SupplierId);
            using (var ctx = new AuditContext())
            {
                if (wholesaler==null)
                {
                    wholesaler = new Domain.Target.InnoAudit.Entities.Wholesaler();
                    wholesaler.Id = sale.SupplierId;
                    ctx.Wholesalers.Add(wholesaler);
                }
                wholesaler.Name = sale.SupplierName;

                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception) { }
            }
            return wholesaler;
        }

        public static Wholesaler InsertOrUpdate(Domain.Source.Sales.Entities.SaleDetailExtension ext)
        {
            return Wholesaler.InsertOrUpdate(ext.Lazy.Sale);
        }

        #endregion
    }
}
