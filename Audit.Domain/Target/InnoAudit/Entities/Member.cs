﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("Members", Schema = "afu")]
    public class Member : BaseEntity
    {
        #region properties

        [Required, MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(10)]
        public string CRMAccountId { get; set; }

        [MaxLength(150)]
        public string Group { get; set; }

        [MaxLength(100)]
        public string FacilityType { get; set; }

        [MaxLength(9)]
        public string DEA { get; set; }

        [MaxLength(15)]
        public string HIN { get; set; }

        [MaxLength(10)]
        public string PremierEIN { get; set; }

        #endregion

        #region static functionality

        public static Member GetById(Guid id)
        {
            using (var ctx = new AuditContext())
            {
                return ctx.Members.SingleOrDefault(x => x.Id == id);
            }
        }

        public static Member InsertOrUpdate(Domain.Source.InnovatixDW.Entities.Member sourceMember)
        {
            var member = Member.GetById(sourceMember.Id);
            using (var ctx = new AuditContext())
            {
                if (member==null)
                {
                    member = new Domain.Target.InnoAudit.Entities.Member();
                    member.Id = member.Id;
                    ctx.Members.Add(member);
                }

                member.CRMAccountId = sourceMember.CRMAccountNumber;
                member.DEA = sourceMember.DEA;
                member.FacilityType = sourceMember.FacilityType;
                member.Group = sourceMember.Group;
                member.HIN = sourceMember.HIN;
                member.Id = sourceMember.Id;
                member.Name = sourceMember.Name;
                member.PremierEIN = sourceMember.PremierEIN;

                try
                {
                    ctx.SaveChanges();
                }
                catch (Exception) { }
            }

            return member;
        }

        public static Member InsertOrUpdate(Domain.Source.Sales.Entities.SaleDetailExtension ext)
        {
            return Member.InsertOrUpdate(ext.Lazy.Member);
        }

        #endregion
    }
}
