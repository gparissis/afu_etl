﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Target.InnoAudit.Entities
{
    [Table("InvoiceLineStatuses", Schema = "afu")]
    public class InvoiceLineStatus : BaseCreatedUpdatedByEntity
    {
        #region properties

        [Required]
        public Guid InvoiceLineId { get; set; }

        [Required]
        public InvoiceStatusEnum Status { get; set; }

        #endregion

        #region functionality

        public InvoiceLineStatusComment InsertComment(string content, CommentSource source, DateTime createdAt)
        {
            using (var ctx = new AuditContext())
            {
                var comment = new InvoiceLineStatusComment();
                comment.Content = content;
                comment.CreatedAt = createdAt;
                comment.Id = Guid.NewGuid();
                comment.InvoiceLineStatusId = this.Id;
                comment.IsVisibleToMember = false;
                comment.IsVisibleToWholesaler = true;
                comment.Source = source;

                ctx.InvoiceLineStatusComments.Add(comment);
                ctx.SaveChanges();
                return comment;
            }
        }

        #endregion
    }
}
