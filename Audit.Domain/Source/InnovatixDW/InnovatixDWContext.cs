﻿using Audit.Domain.Source.InnovatixDW.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW
{
    public class InnovatixDWContext : DbContext
    {
        public DbSet<Member> Members { get; set; }
        public DbSet<ContractItem> ContractItems { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<WholesalerMember> MembersAtWholesalers { get; set; }
        public DbSet<ContractMember> ContractMembers { get; set; }
        public DbSet<DrugPrice> DrugPrices { get; set; }
        public DbSet<Contract> Contracts { get; set; }

        public InnovatixDWContext() : base("innovatix_db")
        {
            Database.SetInitializer<InnovatixDWContext>(null);
        }
    }
}
