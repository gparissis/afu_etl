﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("MemberWholesaler")]
    public class WholesalerMember
    {
        [Key, Column("MemberSupplierID", Order = 0)]
        public Guid MemberSupplierId { get; set; }

        [Column("void")]
        public string Void { get; set; }

        [Column("Audit")]
        public string Audit { get; set; }

        [Column("LoadInnovatixEssensaPharmacy")]
        public string LoadInnovatixEssensaPharmacy { get; set; }

        [Key, Column("MemberWholesalerAccountNumberID", Order = 1)]
        public Guid? MemberWholesalerId { get; set; }

        [Column("AccountNo")]
        public string AccountNumber { get; set; }

        [Column("MemberLevel")]
        public string MemberLevel { get; set; }
    }
}
