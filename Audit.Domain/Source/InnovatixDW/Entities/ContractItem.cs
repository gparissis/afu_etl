﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("ContractItem")]
    public class ContractItem
    {
        [Key, Column("ContractItemId")]
        public int Id { get; set; }

        [Column("ItemId")]
        public int ItemId { get; set; }

        [Column("price")]
        public decimal Price { get; set; }

        [Column("VoidedBatchDetailId")]
        public int? VoidedBatchDetailId { get; set; }

    }
}
