﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("Item")]
    public class Item
    {
        #region properties
        [Key, Column("ItemId")]
        public int Id { get; set; }

        [Column("NDC")]
        public string NDC { get; set; }

        [Column("Tradename")]
        public string Name { get; set; }

        [Column("Form")]
        public string Form { get; set; }

        [Column("Strength")]
        public string Strength { get; set; }

        [Column("Labeler")]
        public string Labeler { get; set; }

        [Column("PackageSize")]
        public string PackageSize { get; set; }

        [Column("UPC")]
        public string UPC { get; set; }

        [Column("VendorNumber")]
        public string NumberAtVendor { get; set; }

        [Column("WAC")]
        public decimal? WAC { get; set; }
        #endregion

        #region functionality

        public DrugPrice GetWACPriceAtInvoiceDate(DateTime invoiceDate)
        {
            using (var ctx = new InnovatixDWContext())
            {
                return ctx.DrugPrices.SingleOrDefault(x => x.Id == Id && x.Code == "WAC" && x.Start.HasValue && x.Start.Value <= invoiceDate && (!x.End.HasValue || x.End.Value >= invoiceDate));
            }
        }

        #endregion

    }
}
