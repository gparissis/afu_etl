﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("ContractMember")]
    public class ContractMember
    {
        [Key, Column("ContractMemberId")]
        public int Id { get; set; }

        [Column("IsVoided")]
        public string IsVoided { get; set; }
    }
}
