﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("DrugPrice")]
    public class DrugPrice
    {
        [Key, Column("DrugPriceId")]
        public int Id { get; set; }

        [Column("ItemId")]
        public int? ItemId { get; set; }

        [Column("Code")]
        public string Code { get; set; }

        [Column("Price")]
        public decimal Price { get; set; }

        [Column("Start")]
        public DateTime? Start { get; set; }

        [Column("End")]
        public DateTime? End { get; set; }
    }
}
