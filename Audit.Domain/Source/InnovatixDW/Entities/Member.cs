﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("Member")]
    public class Member
    {
        [Key, Column("MemberId")]
        public Guid Id { get; set; }

        [Column("BusinessName")]
        public string Name { get; set; }

        [Column("Address1")]
        public string Street1 { get; set; }

        [Column("Address2")]
        public string Street2 { get; set; }
        
        [Column("City")]
        public string City { get; set; }

        [Column("Zip")]
        public string Zip { get; set; }

        [Column("State")]
        public string State { get; set; }

        [Column("Country")]
        public string Country { get; set; }

        [Column("MemberOf")]
        public string MemberOf { get; set; }

        [Column("TerminationDate")]
        public DateTime? End { get; set; }

        [Column("MemberGBM_ID")]
        public string CRMAccountNumber { get; set; }

        [Column("ParentGroup")]
        public string Group { get; set; }

        [Column("FacilityType")]
        public string FacilityType { get; set; }

        [Column("DEA")]
        public string DEA { get; set; }

        [Column("HIN")]
        public string HIN { get; set; }

        [Column("Premier_EIN")]
        public string PremierEIN { get; set; }
    }
}
