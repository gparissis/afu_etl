﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.InnovatixDW.Entities
{
    [Table("Contract")]
    public class Contract
    {
        [Key, Column("ContractId")]
        public int Id { get; set; }

        [Column("ContractType")]
        public string Type { get; set; }

        [Column("ContractNumber")]
        public string Number { get; set; }

        [Column("OwnerContractNumber")]
        public string OwnerNumber { get; set; }

        [Column("Start")]
        public DateTime Start { get; set; }

        [Column("End")]
        public DateTime End { get; set; }

        [Column("Category")]
        public string Category { get; set; }

        [Column("cn_cnid")]
        public string CN_CNID { get; set; }
    }
}
