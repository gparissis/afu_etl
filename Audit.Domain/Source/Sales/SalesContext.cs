﻿using Audit.Domain.Source.Sales.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales
{  

    public class SalesContext : DbContext
    {
        public DbSet<Sale> Sales { get; set; }
        public DbSet<SaleDetail> SaleDetails { get; set; }
        public DbSet<SaleDetailExtension> SaleDetailExtensions { get; set; }
        public DbSet<AuditBatch> AuditBatches { get; set; }
        public DbSet<AuditAction> AuditActions { get; set; }
        public DbSet<PurchaseType> SaleTypes { get; set; }
        public DbSet<SalesMappingArchive> SalesMappingArchives { get; set; }
        public DbSet<DiscrepancyFile> DiscrepancyFiles { get; set; }
        public DbSet<SentDiscrepancy> SentDiscrepancies { get; set; }
        public DbSet<ReceivedDiscrepancy> ReceivedDiscrepancies { get; set; }

        public SalesContext() : base("sales_db")
        {
            Database.SetInitializer<SalesContext>(null);
        }
    }
}
