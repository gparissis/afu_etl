﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("SalesDetail")]
    public class SaleDetail
    {
        [Key, Column("SalesDetailID")]
        public int Id { get; set; }

        [Column("SalesID")]
        public int SaleId { get; set; }

        [Column("PurchaseType")]
        public byte PurchaseTypeId { get; set; }

        [Column("MemberID")]
        public Guid MemberId { get; set; }

        [Column("InvoiceNo")]
        public string InvoiceNumber { get; set; }

        [Column("InvoiceDate")]
        public DateTime InvoiceDate { get; set; }

        [Column("SalesAmount")]
        public decimal InvoiceAmount { get; set; }

        [Column("Units")]
        public int Quantity { get; set; }

        [Column("UnitPrice")]
        public decimal ItemPrice { get; set; }

        [Column("AuditStatus")]
        public int? AuditStatus { get; set; }

        [Column("MemberWholesalerAccountID")]
        public Guid? MemberWholesalerAccountId { get; set; }

        [Column("CreatedDateTime")]
        public DateTime CreatedAt { get; set; }

        [Column("PricingDate")]
        public DateTime? PricingDate { get; set; }

        [Column("SalesMappingID")]
        public int? SalesMappingArchiveId { get; set; }

        [Column("WholesalerItemNumber")]
        public string ItemNumberAtWholesaler { get; set; }
    }
}
