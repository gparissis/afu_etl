﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("SalesFile")]
    public class Sale
    {
        [Key, Column("SalesID")]
        public int Id { get; set; }

        [Column("SupplierID")]
        public Guid SupplierId { get; set; }

        [Column("SupplierName")]
        public string SupplierName { get; set; }
    }
}
