﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("AuditAction")]
    public class AuditAction
    {
        [Key, Column("AuditActionID")]
        public int Id { get; set; }

        [Column("VersionID")]
        public int Version { get; set; }

        [Column("AnalysisCode")]
        public int AnalysisCode{ get; set; }

        [Column("Report")]
        public string Report { get; set; }

        [Column("MajorSection")]
        public string MajorSection { get; set; }

    }
}
