﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("WholesalerAuditResponseDetailReceived")]
    public class ReceivedDiscrepancy
    {
        #region properties

        [Key, Column("WholesalerAuditResponseDetailReceivedID")]
        public int Id { get; set; }

        [Column("WholesalerAuditResponseMasterID")]
        public int FileId { get; set; }

        [Column("ResolutionFlag")]
        public string Resolved { get; set; }

        [Column("CRRBIssued")]
        public string CreditOrRebillIssued { get; set; }

        [Column("CRRBDate")]
        public string CreditDate { get; set; }

        [Column("CRRBRefNum")]
        public string CreditReference { get; set; }

        [Column("CreditAmount")]
        public decimal? CreditAmount { get; set; }

        [Column("CreditQty")]
        public string CreditQuantity { get; set; }

        [Column("RebillDate")]
        public string RebillDate { get; set; }

        [Column("RebillReferenceNumber")]
        public string RebillReference { get; set; }

        [Column("RebillAmount")]
        public decimal? RebillAmount { get; set; }

        [Column("RebillQty")]
        public string RebillQuantity { get; set; }

        [Column("NetCreditRebillAmount")]
        public decimal? NetCreditRebillAmount { get; set; }

        [Column("SalesDetailExtensionID")]
        public long SalesDetailExtensionId { get; set; }

        [Column("WholesalersComments")]
        public string TheirComment { get; set; }

        [NotMapped]
        public bool IsResolved
        {
            get
            {
                switch (Resolved ?? string.Empty)
                {                    
                    case "Y":
                        return true;
                    case "y":
                        return true;
                    case "yes":
                        return true;
                    default:
                        return false;
                }
            }
        }

        [NotMapped]
        public bool IsCreditOrRebillIssued
        {
            get
            {
                switch (CreditOrRebillIssued ?? string.Empty)
                {                    
                    case "true":
                        return true;
                    case "y":
                        return true;
                    case "Yes":
                        return true;
                    default:
                        return false;
                }
            }
        }

        [NotMapped]
        public DateTime? GetCreditOrRebillDate
        {
            get
            {
                DateTime output;
                if (DateTime.TryParse(CreditDate, out output))
                    return output;

                return null;
            }
        }
        #endregion

    }
}
