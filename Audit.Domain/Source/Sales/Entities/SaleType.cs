﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("PurchaseType")]
    public class PurchaseType
    {
        [Key, Column("SalesTypeID")]
        public byte Id { get; set; }

        [Column("SalesType")]
        public string Type { get; set; }

        [Column("Description")]
        public string Name { get; set; }
    }
}
