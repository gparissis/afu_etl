﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("SalesMappingArchive")]
    public class SalesMappingArchive
    {
        [Column("SalesMappingID")]
        public int Id { get; set; }

        [Column("VendorContract")]
        public string VendorContract { get; set; }
    }
}
