﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("SalesDetailExtension")]
    public class SaleDetailExtension
    {
        #region properties

        [Key, Column("SalesDetailExtensionID")]
        public Int64 Id { get; set; }

        [Column("AuditBatchID")]
        public int AuditBatchId { get; set; }

        [Column("SalesDetailID")]
        public Int64 SaleDetailId { get; set; }

        [Column("AnalysisCode")]
        public int? AnalysisCode { get; set; }

        [Column("Analysis")]
        public string Analysis { get; set; }

        [Column("Contractible")]
        public string Contractible { get; set; }

        [Column("ContractIdOnPricingDate")]
        public int? ContractId { get; set; }

        [Column("ContractItemIdOnPricingDate")]
        public int? ContractItemId { get; set; }

        [Column("ContractMemberIdOnPricingDate")]
        public int? ContractMemberId { get; set; }

        [NotMapped]
        public SaleDetailExtensionExtra Lazy { get; set; }

        public SaleDetailExtension()
        {
            Lazy = new SaleDetailExtensionExtra(this);
        }

        #endregion

        #region static functionality

        public static List<SaleDetailExtension> GetExtensionsByAuditDate(DateTime auditDate)
        {
            using (var ctx = new SalesContext())
            {
                return (from e in ctx.SaleDetailExtensions
                        join b in ctx.AuditBatches on e.AuditBatchId equals b.Id
                        where DbFunctions.TruncateTime(b.AuditedAt) == auditDate
                        select e).AsNoTracking().ToList();
            }
        }

        public static SaleDetailExtension GetById(long id)
        {
            using (var ctx = new SalesContext())
            {
                return ctx.SaleDetailExtensions.SingleOrDefault(x => x.Id == id);
            }
        }

        #endregion

        #region functionality

        public AuditBatch GetAuditBatch()
        {
            using (var ctx = new SalesContext())
            {
                return ctx.AuditBatches.Single(x => x.Id == AuditBatchId);
            }
        }

        public Domain.Source.InnovatixDW.Entities.Contract GetOurContract()
        {
            using (var ctx = new Domain.Source.InnovatixDW.InnovatixDWContext())
            {
                return ctx.Contracts.SingleOrDefault(x => x.Id == ContractId);
            }
        }

        public SalesMappingArchive GetVendorContract()
        {
            if (!Lazy.SaleDetail.SalesMappingArchiveId.HasValue) return null;
            using (var ctx = new SalesContext())
            {
                return ctx.SalesMappingArchives.SingleOrDefault(x => x.Id == Lazy.SaleDetail.SalesMappingArchiveId.Value);
            }
        }

        public Domain.Source.InnovatixDW.Entities.WholesalerMember GetMemberAtWholesaler()
        {
            using (var ctx = new InnovatixDW.InnovatixDWContext())
            {
                return ctx.MembersAtWholesalers.SingleOrDefault(x => x.MemberWholesalerId.Value == Lazy.SaleDetail.MemberWholesalerAccountId);
            }
        }

        public Domain.Source.Sales.Entities.PurchaseType GetPurchaseType()
        {
            using (var ctx = new SalesContext())
            {
                return ctx.SaleTypes.SingleOrDefault(x => x.Id == Lazy.SaleDetail.PurchaseTypeId);
            }
        }

        public Domain.Source.InnovatixDW.Entities.ContractMember GetContractMember()
        {
            using (var ctx = new InnovatixDW.InnovatixDWContext())
            {
                return ctx.ContractMembers.SingleOrDefault(x => x.Id == ContractMemberId);
            }
        }

        #endregion
    }

    public class SaleDetailExtensionExtra
    {
        private SaleDetailExtension _Ext;
        private SaleDetail _SaleDetail;
        private Sale _Sale;
        private Domain.Source.InnovatixDW.Entities.ContractItem _ContractItem;
        private Domain.Source.InnovatixDW.Entities.Member _Member;
        private Domain.Source.InnovatixDW.Entities.Item _Item;

        public SaleDetailExtensionExtra(SaleDetailExtension ext)
        {
            _Ext = ext;
        }

        public Sale Sale
        {
            get
            {
                if (_Sale == null)
                {
                    using (var ctx = new Domain.Source.Sales.SalesContext())
                    {
                        var detail = _Ext.Lazy.SaleDetail;
                        if (detail != null)
                            _Sale = ctx.Sales.SingleOrDefault(x => x.Id == detail.SaleId);
                        else
                            _Sale = null;
                    }
                }
                return _Sale;
            }
        }

        public SaleDetail SaleDetail
        {
            get
            {
                if (_SaleDetail == null)
                {
                    using (var ctx = new Domain.Source.Sales.SalesContext())
                    {
                        _SaleDetail = ctx.SaleDetails.SingleOrDefault(x => x.Id == _Ext.SaleDetailId);
                    }
                }
                return _SaleDetail;
            }
        }

        public Domain.Source.InnovatixDW.Entities.ContractItem ContractItem
        {
            get
            {
                if (_ContractItem == null)
                {
                    if (!_Ext.ContractItemId.HasValue)
                        _ContractItem = null;
                    else
                    {
                        using (var ctx = new Domain.Source.InnovatixDW.InnovatixDWContext())
                        {
                            _ContractItem = ctx.ContractItems.SingleOrDefault(x => x.Id == _Ext.ContractItemId.Value);
                        }
                    }
                }
                return _ContractItem;
            }
        }

        public Domain.Source.InnovatixDW.Entities.Item Item
        {
            get
            {
                if (_Item == null)
                {
                    using (var ctx = new Domain.Source.InnovatixDW.InnovatixDWContext())
                    {
                        if (_Ext.Lazy.ContractItem != null)
                            _Item = ctx.Items.SingleOrDefault(x => x.Id == _Ext.Lazy.ContractItem.ItemId);
                        else
                            _Item = null;
                    }
                }
                return _Item;
            }
        }

        public Domain.Source.InnovatixDW.Entities.Member Member
        {
            get
            {
                if (_Member == null)
                {
                    using (var ctx = new Domain.Source.InnovatixDW.InnovatixDWContext())
                    {
                        _Member = ctx.Members.SingleOrDefault(x => x.Id == _Ext.Lazy.SaleDetail.MemberId);
                    }
                }
                return _Member;
            }
        }
    }
}
