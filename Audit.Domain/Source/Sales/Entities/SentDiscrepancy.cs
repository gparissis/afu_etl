﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("WholesalerAuditResponseDetailSent")]
    public class SentDiscrepancy
    {
        [Key, Column("WholesalerAuditResponseDetailSentID")]
        public int Id { get; set; }

        [Column("WholesalerAuditResponseMasterID")]
        public int FileId { get; set; }

        [Column("SalesDetailExtensionID")]
        public long SalesDetailExtensionId { get; set; }

        [Column("InnovatixComments")]
        public string OurComment { get; set; }
    }
}
