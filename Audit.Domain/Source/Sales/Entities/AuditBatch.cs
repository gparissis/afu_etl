﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("AuditBatch")]
    public class AuditBatch
    {
        [Key, Column("AuditBatchID")]
        public int Id { get; set; }

        [Column("AuditDateTime")]
        public DateTime AuditedAt { get; set; }
    }
}
