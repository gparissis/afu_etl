﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audit.Domain.Source.Sales.Entities
{
    [Table("WholesalerAuditResponseMaster")]
    public class DiscrepancyFile
    {
        #region properties

        [Key, Column("WholesalerAuditResponseMasterID")]
        public int Id { get; set; }

        [Column("CorrespondenceDateTime")]
        public DateTime? DateSentOrReceived { get; set; }

        [Column("CorrespondenceType")]
        public string SentOrReceived { get; set; }

        #endregion

        #region static functionality

        public static List<DiscrepancyFile> GetAll()
        {
            using (var ctx = new SalesContext())
            {
                return ctx.DiscrepancyFiles.AsNoTracking().OrderBy(x => x.DateSentOrReceived).ToList();
            }
        }

        public static List<DiscrepancyFile> GetGreaterThanId(int id)
        {
            using (var ctx = new SalesContext())
            {
                return ctx.DiscrepancyFiles.AsNoTracking().Where(x => x.Id > id).OrderBy(x => x.DateSentOrReceived).ToList();
            }
        }

        #endregion

        #region functionality

        public List<SentDiscrepancy> GetSentRows()
        {
            using (var ctx = new SalesContext())
            {
                return ctx.SentDiscrepancies.AsNoTracking().Where(x => x.FileId == Id).ToList();
            }
        }

        public List<ReceivedDiscrepancy> GetReceivedRows()
        {
            using (var ctx = new SalesContext())
            {
                return ctx.ReceivedDiscrepancies.AsNoTracking().Where(x => x.FileId == Id).ToList();
            }
        }
        #endregion
    }
}
