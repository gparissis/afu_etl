﻿CREATE TABLE [afu].[InvoiceLines]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [DetailId] BIGINT NOT NULL,
	[AuditId] BIGINT NOT NULL, 
    [MemberId] UNIQUEIDENTIFIER NOT NULL, 
    [WholesalerId] UNIQUEIDENTIFIER NOT NULL, 
	[ItemId] UNIQUEIDENTIFIER NOT NULL, 
    [MemberAccountNumberByWholesaler] NVARCHAR(20) NULL, 
    [MemberLevelAtWholesaler] NVARCHAR(10) NULL,
	[ItemNumberAtVendor] NVARCHAR(50) NULL,  
    [ItemNumberAtWholesaler] NVARCHAR(50) NULL, 
    [InvoiceNumber] NVARCHAR(50) NULL, 
    [InvoiceDate] DATETIME NOT NULL, 
    [PricingDate] DATETIME NULL, 
    [PurchaseType] NVARCHAR(50) NOT NULL, 
    [Quantity] INT NOT NULL, 
    [WACOnInvoiceDate] DECIMAL(10, 2) NULL, 
    [ContractPriceByInnovatix] DECIMAL(10, 2) NULL, 
    [UnitPrice] DECIMAL(10, 2) NULL, 
    [Difference] DECIMAL(10, 2) NOT NULL, 
    [ItemTotalPrice] DECIMAL(10, 2) NOT NULL, 
    [ContractNumberByVendor] NVARCHAR(15) NULL, 
    [ContractNumberByInnovatix] NVARCHAR(50) NULL, 
    [AuditDate] DATETIME NOT NULL, 
	[AnalysisCode] INT NOT NULL,
    [Analysis] NVARCHAR(255) NULL, 
	[OriginalAnalysis] NVARCHAR(255) NULL,
    [IsEligibilityVerified] BIT NOT NULL, 
    [IsPriceVerified] BIT NOT NULL, 
    [IsCreditOrRebillIssued] BIT NOT NULL, 
    [CreditOrRebillDate] DATETIME NULL, 
    [CreditOrRebillReferenceNumber] NVARCHAR(50) NULL, 
    [LastStatus] TINYINT NOT NULL, 
	[LastActivity] DATETIME2(0) NOT NULL,
	[LastActivitySource] TINYINT NOT NULL,
	[LastInnovatixComment] NVARCHAR(255),
	[LastWholesalerComment] NVARCHAR(255),
    [CreatedAt] DATETIME2(7) NOT NULL, 
	[CreatedBy] NVARCHAR(50) NULL,
    [UpdatedAt] DATETIME2(7) NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NULL, 
    [BatchId] UNIQUEIDENTIFIER NULL, 
    [ManualExcluded] BIT NOT NULL DEFAULT 0, 
    [LastInnovatixActivity] DATETIME2(0) NULL, 
    [LastWholesalerActivity] DATETIME2(0) NULL, 
    [AutoIncrement] INT NOT NULL IDENTITY(1,1), 
    CONSTRAINT [FK_InvoiceLines_ToWholesalers] FOREIGN KEY ([WholesalerId]) REFERENCES [afu].[Wholesalers]([Id]), 
    CONSTRAINT [FK_InvoiceLines_ToMembers] FOREIGN KEY ([MemberId]) REFERENCES [afu].[Members]([Id]), 
    CONSTRAINT [FK_InvoiceLines_ToItems] FOREIGN KEY ([ItemId]) REFERENCES [afu].[Items]([Id]), 
    CONSTRAINT [PK_InvoiceLines_Id] PRIMARY KEY NONCLUSTERED ([Id]), 
    CONSTRAINT [FK_InvoiceLines_ToBatches] FOREIGN KEY ([BatchId]) REFERENCES [afu].[Batches]([Id])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The record id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The sales detail extension record id in Sales database.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'AuditId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The member id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'MemberId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The wholesaler id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'WholesalerId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The account number in wholesaler''s system.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'MemberAccountNumberByWholesaler'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The member level at the wholesaler.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'MemberLevelAtWholesaler'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The item''s number in wholesaler''s system.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'ItemNumberAtWholesaler'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The invoice number.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'InvoiceNumber'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date of the invoice.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'InvoiceDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date of the pricing.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'PricingDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The type of the sale.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'PurchaseType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Units purchased.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'Quantity'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The wholesaler''s WAC price of the item.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'WACOnInvoiceDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The contract price of the item, based on Innovatix.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'ContractPriceByInnovatix'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The price per unit that was invoiced.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'UnitPrice'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'(Item price invoiced - Innovatix contract price suggestion) * quantity.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'Difference'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Unit price * quantity.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'ItemTotalPrice'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The reported vendor''s contract number, that pricing was based on.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'ContractNumberByVendor'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The reported contract number, by Innovatix, that pricing should be based on.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'ContractNumberByInnovatix'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date the audit was created.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'AuditDate'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The analysis of this discrepancy.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'Analysis'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was created.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'CreatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was last updated.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'UpdatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The status of the invoice. 0:Open, 1: Pending, 3:Resolved, 4:Closed. This is a redundant value for COGNOS reports.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = 'LastStatus'
GO

CREATE CLUSTERED INDEX [IX_InvoiceLines_CreatedAt] ON [afu].[InvoiceLines] ([CreatedAt])

GO

CREATE INDEX [IX_InvoiceLines_InvoiceDate] ON [afu].[InvoiceLines] ([InvoiceDate])

GO

CREATE INDEX [IX_InvoiceLines_MemberId] ON [afu].[InvoiceLines] ([MemberId])

GO

CREATE INDEX [IX_InvoiceLines_WholesalerId] ON [afu].[InvoiceLines] ([WholesalerId])

GO

CREATE INDEX [IX_InvoiceLines_ItemId] ON [afu].[InvoiceLines] ([ItemId])

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The item id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'ItemId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The analysis of this discrepancy.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'OriginalAnalysis'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The analysis code of this discrepancy.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'AnalysisCode'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The item''s number for the vendor.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'ItemNumberAtVendor'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The sale details record id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'DetailId'
GO

CREATE INDEX [IX_InvoiceLines_DetailId] ON [afu].[InvoiceLines] ([DetailId])
GO

CREATE INDEX [IX_InvoiceLines_AuditId] ON [afu].[InvoiceLines] ([AuditId])

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The last date and time something changed regarding the invoice.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastActivity'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The last Innovatix comment.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastInnovatixComment'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The last wholesaler comment.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastWholesalerComment'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'0: System origin, 1: Innovatix employee, 2: Wholesaler',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastActivitySource'
GO

CREATE INDEX [IX_InvoiceLines_BatchId] ON [afu].[InvoiceLines] ([BatchId])

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'When was the last time Innovatix altered the state of the row or other rows dependent on this row.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastInnovatixActivity'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'When was the last time Wholesaler altered the state of the row or other rows dependent on this row.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'InvoiceLines',
    @level2type = N'COLUMN',
    @level2name = N'LastWholesalerActivity'