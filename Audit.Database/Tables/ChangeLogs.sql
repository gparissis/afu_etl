﻿CREATE TABLE [afu].[ChangeLogs]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Entity] NVARCHAR(50) NOT NULL, 
    [EntityId] UNIQUEIDENTIFIER NOT NULL,
	[ChangeType] NVARCHAR(1) NOT NULL, 
    [OldValues] NVARCHAR(MAX) NULL, 
    [NewValues] NVARCHAR(MAX) NULL, 
    [ChangedProperties] NVARCHAR(MAX) NULL, 
    [CreatedAt] DATETIME2 NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_ChangeLogs] PRIMARY KEY NONCLUSTERED ([Id])
)

GO

CREATE CLUSTERED INDEX [IX_ChangeLogs_CreatedAt] ON [afu].[ChangeLogs] ([CreatedAt])
