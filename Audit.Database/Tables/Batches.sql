﻿CREATE TABLE [afu].[Batches]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Title] NVARCHAR(50) NOT NULL, 
    [WholesalerId] UNIQUEIDENTIFIER NOT NULL, 
    [InvoicedFrom] DATETIME NOT NULL, 
    [InvoicedUntil] DATETIME NOT NULL, 
    [SendTo] NVARCHAR(250) NOT NULL, 
    [Pending] SMALLINT NOT NULL, 
    [Closed] SMALLINT NOT NULL, 
    [Resolved] SMALLINT NOT NULL, 
	[IsSend] BIT NOT NULL,
    [CreatedAt] DATETIME2 NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedAt] DATETIME2 NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_Batches] PRIMARY KEY NONCLUSTERED ([Id]), 
    CONSTRAINT [FK_Batches_ToWholesalers] FOREIGN KEY ([WholesalerId]) REFERENCES [afu].[Wholesalers]([Id])
)

GO

CREATE CLUSTERED INDEX [IX_Batches_CreatedAt] ON [afu].[Batches] ([CreatedAt])

GO

CREATE INDEX [IX_Batches_WholesalerId] ON [afu].[Batches] ([WholesalerId])

GO

CREATE INDEX [IX_Batches_CreatedBy] ON [afu].[Batches] ([CreatedBy])
