﻿CREATE TABLE [afu].[Members]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Name] NVARCHAR(100) NOT NULL, 
    [CRMAccountId] NVARCHAR(10) NULL,
	[Group] NVARCHAR(150) NULL,
	[FacilityType] NVARCHAR(100) NULL, 
	[DEA] NVARCHAR(9) NULL,
	[HIN] NVARCHAR(15) NULL,
	[PremierEIN] NVARCHAR(10) NULL,
    [CreatedAt] DATETIME2 NOT NULL, 
    [UpdatedAt] DATETIME2 NOT NULL, 
    CONSTRAINT [PK_Members_Id] PRIMARY KEY NONCLUSTERED ([Id])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The record id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The name of the wholesaler.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was created.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'CreatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was last updated. When record was created, before the first update, it has the same value with CreatedAt field.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'UpdatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The account number of this member in CRM. (At the time of writing i don''t know what CRM they mean)',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = 'CRMAccountId'
GO

CREATE CLUSTERED INDEX [IX_Members_CreatedAt] ON [afu].[Members] ([CreatedAt])

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The group of the member. (Need more research what exactly means.)',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'Group'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The facility type of the member. (Need more research what it means.)',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'FacilityType'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Drug Enforcement Administration number.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = N'DEA'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'Health Industry Number.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = 'HIN'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The member''s EIN number in Premier.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Members',
    @level2type = N'COLUMN',
    @level2name = 'PremierEIN'
GO
