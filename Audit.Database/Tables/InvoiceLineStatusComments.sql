﻿CREATE TABLE [afu].[InvoiceLineStatusComments]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [InvoiceLineStatusId] UNIQUEIDENTIFIER NOT NULL, 
    [Source] TINYINT NOT NULL, 
    [Content] NVARCHAR(255) NOT NULL, 
    [IsVisibleToWholesaler] BIT NOT NULL, 
    [IsVisibleToMember] BIT NOT NULL, 
    [CreatedAt] DATETIME2(7) NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedAt] DATETIME2(7) NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_InvoiceLineStatusComments_ToInvoiceLineStatuses] FOREIGN KEY ([InvoiceLineStatusId]) REFERENCES [afu].[InvoiceLineStatuses]([Id]), 
    CONSTRAINT [PK_InvoiceLineStatusComments_Id] PRIMARY KEY NONCLUSTERED ([Id])
)

GO

CREATE CLUSTERED INDEX [IX_InvoiceLineStatusComments_CreatedAt] ON [afu].[InvoiceLineStatusComments] ([CreatedAt])

GO

CREATE INDEX [IX_InvoiceLineStatusComments_InvoiceLineStatusId] ON [afu].[InvoiceLineStatusComments] ([InvoiceLineStatusId])
