﻿CREATE TABLE [afu].[Items]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [SourceId] INT NOT NULL, 
    [Tradename] NVARCHAR(100) NULL, 
    [NDC] NVARCHAR(11) NULL, 
	[UPC] NVARCHAR(12) NULL, 
    [LabelerAbbreviation] NVARCHAR(100) NULL, 
    [Form] NVARCHAR(60) NULL, 
    [Strength] NVARCHAR(60) NULL, 
    [PackageSize] NVARCHAR(50) NULL, 
	[VendorNumber] NVARCHAR(100) NULL,
    [CreatedAt] DATETIME2 NOT NULL, 
    [UpdatedAt] DATETIME2 NOT NULL, 
    CONSTRAINT [PK_Items_Id] PRIMARY KEY NONCLUSTERED ([Id]),

)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The record id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The record id at InnovatixDW.Items.ItemId.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'SourceId'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The tradename of the item.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'Tradename'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The NDC.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'NDC'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The UPC.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'UPC'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The item''s laberer abbreviation.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'LabelerAbbreviation'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The form of the item.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'Form'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The strength.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'Strength'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The package of the item.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'PackageSize'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was created.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'CreatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was last updated.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'UpdatedAt'
GO

CREATE CLUSTERED INDEX [IX_Items_CreatedAt] ON [afu].[Items] ([CreatedAt])

GO

CREATE UNIQUE INDEX [IX_Items_SourceId] ON [afu].[Items] ([SourceId])

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The number of the item in the vendor''s system.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Items',
    @level2type = N'COLUMN',
    @level2name = N'VendorNumber'