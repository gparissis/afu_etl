﻿CREATE TABLE [afu].[WholesalerExclusions]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Rules] NVARCHAR(MAX) NOT NULL, 
    [CreatedAt] DATETIME2(0) NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedAt] DATETIME2(0) NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_WholesalerExclusions] PRIMARY KEY NONCLUSTERED ([Id]), 
    CONSTRAINT [FK_WholesalerExclusions_ToWholesalers] FOREIGN KEY ([Id]) REFERENCES [afu].[Wholesalers]([Id])
)

GO

CREATE CLUSTERED INDEX [IX_WholesalerExclusions_CreatedAt] ON [afu].[WholesalerExclusions] ([CreatedAt])
