﻿CREATE TABLE [afu].[Reconciliations]
(
	[Id] UNIQUEIDENTIFIER NOT NULL, 
	[CreditIssued] BIT NOT NULL,
    [CreditDate] DATE NULL, 
	[CreditReferenceNumber] NVARCHAR(50) NULL,
	[CreditAmount] DECIMAL(8,2) NULL,
	[CreditQuantity] SMALLINT NULL,
	[RebillIssued] BIT NOT NULL,
	[RebillDate] DATE NULL,
	[RebillReferenceNumber] NVARCHAR(50) NULL,
	[RebillAmount] DECIMAL(8,2) NULL,
	[RebillQuantity] SMALLINT NULL,
	[NetCreditRebillAmount] DECIMAL(8,2) NULL,
    [CreatedAt] DATETIME2(7) NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedAt] DATETIME2(7) NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [PK_Reconciliations_ID] PRIMARY KEY NONCLUSTERED ([Id]), 
    CONSTRAINT [FK_Reconciliations_ToInvoiceLines] FOREIGN KEY ([Id]) REFERENCES [afu].[InvoiceLines]([Id]) 
)

GO

CREATE CLUSTERED INDEX [IX_Reconciliations_CreatedAt] ON [afu].[Reconciliations] ([CreatedAt])
