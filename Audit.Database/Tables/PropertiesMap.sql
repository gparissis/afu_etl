﻿CREATE TABLE [afu].[PropertiesMap]
(
	[Table] NVARCHAR(50) NOT NULL,
	[Field] NVARCHAR(50) NOT NULL,
	[Value] NVARCHAR(50) NOT NULL,
	[Description] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [PK_PropertiesMap] PRIMARY KEY ([Table], [Field], [Value])
)
