﻿CREATE TABLE [afu].[Wholesalers]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [Name] NVARCHAR(100) NOT NULL, 
    [CreatedAt] DATETIME2 NOT NULL, 
    [UpdatedAt] DATETIME2 NOT NULL, 
    CONSTRAINT [PK_Wholesalers_Id] PRIMARY KEY NONCLUSTERED ([Id])
)

GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The record id.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Wholesalers',
    @level2type = N'COLUMN',
    @level2name = N'Id'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The name of the wholesaler.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Wholesalers',
    @level2type = N'COLUMN',
    @level2name = N'Name'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was created',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Wholesalers',
    @level2type = N'COLUMN',
    @level2name = N'CreatedAt'
GO
EXEC sp_addextendedproperty @name = N'MS_Description',
    @value = N'The date and time the record was last updated. When record was created, before the first update, it has the same value with CreatedAt field.',
    @level0type = N'SCHEMA',
    @level0name = N'afu',
    @level1type = N'TABLE',
    @level1name = N'Wholesalers',
    @level2type = N'COLUMN',
    @level2name = N'UpdatedAt'
GO

CREATE CLUSTERED INDEX [IX_Wholesalers_CreatedAt] ON [afu].[Wholesalers] ([CreatedAt])
