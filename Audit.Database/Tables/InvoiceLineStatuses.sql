﻿CREATE TABLE [afu].[InvoiceLineStatuses]
(
	[Id] UNIQUEIDENTIFIER NOT NULL , 
    [InvoiceLineId] UNIQUEIDENTIFIER NOT NULL, 
    [Status] TINYINT NOT NULL, 
    [CreatedAt] DATETIME2(7) NOT NULL, 
    [CreatedBy] NVARCHAR(50) NOT NULL, 
    [UpdatedAt] DATETIME2(7) NOT NULL, 
    [UpdatedBy] NVARCHAR(50) NOT NULL, 
    CONSTRAINT [FK_InvoiceStatuses_ToInvoices] FOREIGN KEY ([InvoiceLineId]) REFERENCES [afu].[InvoiceLines]([Id]), 
    CONSTRAINT [PK_InvoiceStatuses_Id] PRIMARY KEY NONCLUSTERED ([Id])
)

GO

CREATE CLUSTERED INDEX [IX_InvoiceStatuses_CreatedAt] ON [afu].[InvoiceLineStatuses] ([CreatedAt])

GO

CREATE INDEX [IX_InvoiceStatuses_InvoiceId] ON [afu].[InvoiceLineStatuses] ([InvoiceLineId])
