﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

print 'Inserting data for PropertiesMap table.'

print '--> Inserting [afu].[InvoiceLines].[LastStatus] values.'

insert afu.PropertiesMap select 'InvoiceLines', 'LastStatus', '0', 'Open. No any action yet.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastStatus' and [Value]='0');

insert afu.PropertiesMap select 'InvoiceLines', 'LastStatus', '1', 'Pending. The process to resolve the line has started.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastStatus' and [Value]='1');

insert afu.PropertiesMap select 'InvoiceLines', 'LastStatus', '2', 'Resolved. The line was resolved.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastStatus' and [Value]='2');

insert afu.PropertiesMap select 'InvoiceLines', 'LastStatus', '3', 'Closed. The line was not resolved and was closed.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastStatus' and [Value]='3');

/*
=======================================================================================================================
*/

print '--> Inserting [afu].[InvoiceLines].[LastActivitySource] values.'

insert afu.PropertiesMap select 'InvoiceLines', 'LastActivitySource', '0', 'System. Last activity was the creation of this record by the system.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastActivitySource' and [Value]='0');

insert afu.PropertiesMap select 'InvoiceLines', 'LastActivitySource', '1', 'Innovatix. Someone from Innovatix did the last action.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastActivitySource' and [Value]='1');

insert afu.PropertiesMap select 'InvoiceLines', 'LastActivitySource', '2', 'Wholesaler. The wholesaler did the last action.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLines' and [Field]='LastActivitySource' and [Value]='2');

/*
=======================================================================================================================
*/

print '--> Inserting [afu].[InvoiceLinesStatuses].[Status] values.'

insert afu.PropertiesMap select 'InvoiceLinesStatuses', 'Status', '0', 'Open. No any action yet.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLinesStatuses' and [Field]='Status' and [Value]='0');

insert afu.PropertiesMap select 'InvoiceLinesStatuses', 'Status', '1', 'Pending. The process to resolve the line has started.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLinesStatuses' and [Field]='Status' and [Value]='1');

insert afu.PropertiesMap select 'InvoiceLinesStatuses', 'Status', '2', 'Resolved. The line was resolved.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLinesStatuses' and [Field]='Status' and [Value]='2');

insert afu.PropertiesMap select 'InvoiceLinesStatuses', 'Status', '3', 'Closed. The line was not resolved and was closed.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLinesStatuses' and [Field]='Status' and [Value]='3');

/*
=======================================================================================================================
*/

print '--> Inserting [afu].[InvoiceLineStatusComments].[Source] values.'

insert afu.PropertiesMap select 'InvoiceLineStatusComments', 'Source', '0', 'Innovatix. Someone from Innovatix created the comment.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLineStatusComments' and [Field]='Source' and [Value]='0');

insert afu.PropertiesMap select 'InvoiceLineStatusComments', 'Source', '1', 'Wholesaler. A wholesaler contact created the comment.'
where not exists (select 1 from afu.PropertiesMap where [Table]='InvoiceLineStatusComments' and [Field]='Source' and [Value]='1');
