﻿CREATE VIEW [afu].[GroupedDateAudits]
AS 
SELECT CAST(CONVERT(VARCHAR(8), AuditDate, 112) AS DATE) AS AtDate, COUNT(*) AS Audited
	FROM [afu].[InvoiceLines]	
	GROUP BY CAST(CONVERT(VARCHAR(8), AuditDate, 112) AS DATE)

