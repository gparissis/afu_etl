﻿CREATE VIEW [afu].[InvoiceLineSummaryRows] WITH SCHEMABINDING AS 
SELECT
	lines.Id as Id,
	lines.WholesalerId as WholesalerId,
	lines.MemberId as MemberId,
	lines.AuditId as AuditId,
	CASE
		WHEN lines.LastStatus = 0 THEN N'OPEN'
		WHEN lines.LastStatus = 1 THEN N'PENDING'
		WHEN lines.LastStatus = 2 THEN N'RESOLVED'
		WHEN lines.LastStatus = 3 THEN N'CLOSED'
	END	AS [Status],
	lines.LastActivity as LastActivity,
	CASE
		WHEN lines.LastActivitySource = 0 THEN N'SYSTEM'
		WHEN lines.LastActivitySource = 1 THEN N'INNOVATIX'
		WHEN lines.LastActivitySource = 2 THEN wholesalers.Name
	END AS [LastActivityBy],
	members.Name as MemberName,
	members.CRMAccountId as CRMAccountNumber,
	members.[Group] as [Group],
	members.FacilityType as FacilityType,
	members.DEA as DEA,
	members.HIN as HIN,
	members.PremierEIN as PremierEIN,
	wholesalers.Name as WholesalerName,
	lines.MemberAccountNumberByWholesaler as MemberAccountNumberAtWholesaler,
	lines.MemberLevelAtWholesaler as MemberLevelAtWholesaler,
	items.LabelerAbbreviation as Labeler,
	items.Tradename as Item,
	items.Form as Form,
	items.Strength as Strength,
	items.PackageSize as PackageSize,
	items.NDC as NDC,
	items.UPC as UPC,
	lines.ItemNumberAtVendor as ItemNumberAtVendor,
	lines.ItemNumberAtWholesaler as ItemNumberAtWholesaler,
	lines.InvoiceNumber as InvoiceNumber,
	lines.InvoiceDate as InvoiceDate,
	lines.PricingDate as PricingDate,
	lines.PurchaseType as PurchaseType,
	lines.Quantity as Quantity,
	lines.WACOnInvoiceDate as WACOnInvoiceDate,
	lines.ContractPriceByInnovatix as ContractPriceByInnovatix,
	lines.UnitPrice as UnitPrice,
	lines.[Difference] as [Difference],
	lines.ItemTotalPrice as ItemTotalPrice,
	lines.ContractNumberByVendor as ContractNumberByVendor,
	lines.ContractNumberByInnovatix as ContractNumberByInnovatix,
	lines.AuditDate as AuditDate,
	lines.Analysis as Analysis,
	lines.LastInnovatixComment as LastInnovatixComment,
	lines.LastWholesalerComment as LastWholesalerComment,
	reconciliations.CreditIssued AS CreditIssued,
	reconciliations.CreditDate AS CreditDate,
	reconciliations.CreditReferenceNumber AS CreditReferenceNumber,
	reconciliations.CreditAmount AS CreditAmount,
	reconciliations.CreditQuantity AS CreditQuantity,
	reconciliations.RebillIssued AS RebillIssued,
	reconciliations.RebillDate AS RebillDate,
	reconciliations.RebillReferenceNumber AS RebillReferenceNumber,
	reconciliations.RebillAmount AS RebillAmount,
	reconciliations.RebillQuantity AS RebillQuantity,
	reconciliations.NetCreditRebillAmount AS NetCreditRebillAmount,
	lines.BatchId AS BatchId,
	lines.ManualExcluded AS ManualExcluded,
	lines.AutoIncrement AS AutoIncrement
FROM [afu].InvoiceLines lines
INNER JOIN [afu].[Members] members ON lines.MemberId=members.Id
INNER JOIN [afu].[Wholesalers] wholesalers on lines.WholesalerId=wholesalers.Id
INNER JOIN [afu].[Items] items ON lines.ItemId=items.Id
INNER JOIN [afu].[Reconciliations] reconciliations ON lines.Id = reconciliations.Id

GO
CREATE UNIQUE CLUSTERED INDEX PK_InvoiceLineSummaryRows ON [afu].[InvoiceLineSummaryRows] ([AutoIncrement])

GO
CREATE INDEX [IX_InvoiceLineSummaryRows_MemberName] ON [afu].[InvoiceLineSummaryRows] ([MemberName])

GO
CREATE INDEX [IX_InvoiceLineSummaryRows_Labeler] ON [afu].[InvoiceLineSummaryRows] ([Labeler])

GO
CREATE INDEX [IX_InvoiceLineSummaryRows_BatchId] ON [afu].[InvoiceLineSummaryRows] ([BatchId])

GO
CREATE INDEX [IX_InvoiceLineSummaryRows_PurchaseType] ON [afu].[InvoiceLineSummaryRows] ([PurchaseType])

GO
CREATE INDEX [IX_InvoiceLineSummaryRows_Group] ON [afu].[InvoiceLineSummaryRows] ([Group])
